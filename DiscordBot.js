import Discord from "discord.js";
import {createRequire} from "module"; // Bring in the ability to create the 'require' method
const require = createRequire(import.meta.url); // construct the require method

export default class DiscordBot {
  constructor(core) {
    this.core = core
    require("dotenv").config();
    const intents = new Discord.Intents(32767);

    this.discordClient = new Discord.Client({ intents });
    this.discordClient.login(process.env.DISCORD_BOT_LOGIN_KEY);
    this.discordClient.on("messageCreate", async message =>{
      try {
        this.onMessageCreate(message)
      } catch (error) {
        console.log(error)
        this.sendMessage(message.channel, `err: ${  error.message}`)
      }
    });
  }
  async onMessageCreate(message) {
    let prefix = process.env.DISCORD_BOT_COMMAND_PREFIX ?? ":"
    const args = message.content.substring(prefix.length).split(/ +/);

    if(!message.content.startsWith(prefix)) return; // should start with command prefix

    if(!message.member.permissions.has("ADMINISTRATOR")){
      if(message.member.user.id !== "328991679916802050"){
        return;
      }
    }

    console.log(`${message.member} called command: ${message.content}`)

    const responseMessage = 
      `:test ; test
      :test2 ; test2`

    switch(args[0]){
    case "help":
    case "getCommands":
    case "commands":
      this.sendMessage(message.channel, responseMessage)
      break;
    case "test":
      this.testList(message.channel)

      if(args[1] && args[2]) {
        //this.testList(message.channel, args[2])
      }

      break;
    }
  }
  sendMessage(channel, messageString) {
    channel = this.channelIdToChannel(channel)
    const messageEmbed = new Discord.EmbedBuilder().setDescription(messageString)

    channel.send({embeds: [messageEmbed]});
  }
  channelIdToChannel(channelId) {
    let channel = null

    if(typeof channelId === "string") {
      //channel = this.discordClient.channels.cache.get(channel);
      channel = this.discordClient.channels.cache.get(channelId);

      if(!channel) {
        console.log(`channel ${channelId} not exist`)

        return null
      }
    }

    return channel
  }
  testList(channel, largeImage = true) {
    channel = this.channelIdToChannel(channel)
    let asset = {
      nameId: "Tieroc",
      token_address: "0xee972ad3b8ac062de2e4d5e6ea4a37e36c849a11",
      buy_token_symbol: "ETH",
      floorValue: {["ETH"]: 0.2014},
      orderERC721: {data: {properties: {image_url: "https://gog-art-assets.s3-ap-southeast-2.amazonaws.com/Content/Thumbnails/Heroes/Tieroc/Thumbnail_Hero_Tieroc_Base.png"}}}
    }

    console.log("listing test...")
    let url; let baseDescription

    baseDescription = "Total supply 1131"
    url = `https://zkmarkets.com/assets/${asset.token_address}/${encodeURIComponent(asset.nameId)}`;
    const assetEmbed = new Discord.EmbedBuilder();
    let floorValueText = asset.floorValue ? `${asset.floorValue[asset.buy_token_symbol]} ${asset.buy_token_symbol} (x$)` : "?"

    let description = `${baseDescription} ${asset.nameId}
    floor ${floorValueText}`
    
    assetEmbed.setColor("#0099ff")
      .setTitle(asset.nameId)
      .setURL  (url)
      .setDescription(description);
    
    let attributes = [
      {
        name: "Rarity",
        value: "Legendary",
        symbol: ":large_orange_diamond:"
      },
      {
        name: "Special Edition",
        value: "Normal",
        symbol: ":white_small_square:"
      },
      {
        name: "Series",
        value: "Founder",
        symbol: ":large_orange_diamond:"
      },
      {
        name: "Element",
        value: "Dark",
        symbol: ":purple_square:"
      },
      {
        name: "Faction",
        value: "Glade",
        symbol: ":purple_square:"
      },
      {
        name: "Class",
        value: "Mage",
        symbol: ":purple_square:"
      },
    ]

    for(let attribute of attributes) {
      assetEmbed.addField(`${attribute.name  }: \n${  attribute.value}` , attribute.symbol, true);  
    }

    if(largeImage) {
      assetEmbed.setImage(asset.orderERC721.data.properties.image_url);
    } else {
      assetEmbed.setThumbnail(asset.orderERC721.data.properties.image_url);
    }

    if(channel.permissionsFor(this.discordClient.user).has(Discord.PermissionsBitField.Flags.SendMessages)) {
      channel.send({embeds: [assetEmbed]});
    } else {
      console.log("no permissions in")
      console.log(channel)
    }
  }
  
  async fetchAllMessages(channel) {
    channel = this.channelIdToChannel(channel)
    let messages = [];
  
    // Create message pointer
    let message = await channel.messages
      .fetch({ limit: 1 })
      .then((messagePage) => {
        return messagePage.size === 1 ? messagePage.at(0) : null
      });
  
    while (message) {
      await channel.messages
        .fetch({ limit: 100, before: message.id })
        .then(messagePage => {
          messagePage.forEach(msg => messages.push(msg));
  
          // Update our message pointer to be last message in page of messages
          message = 0 < messagePage.size ? messagePage.at(messagePage.size - 1) : null;
        })
    }
  
    console.log(messages);  // Print all messages
  }
}