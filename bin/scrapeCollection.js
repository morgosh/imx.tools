// USAGE 
// node bin/scrapeCollection.js $collection_address
// scrapes data from collection
/*
  If the collection is named, then it will bundle the same named assets together and wont treat them as unique, the scraper will count the assets.
  If the collection is not named, then we consider it as a default rarityCollection. The scraper will calculate rarity based on the asset metadata
  We can define named collections core or create collection specific rules with rulesParser
*/

import { Config } from "node-json-db/dist/lib/JsonDBConfig.js"
import ImxCore from "../ImxCore.js"
import { JsonDB } from "node-json-db"
import { rulesParser } from "../rulesParser.js"

let args = process.argv.slice(2)
let collectionAddress = args[0]

if(!collectionAddress) {
  console.log("no collectionAddress")
}

let core = new ImxCore()

await core.getImxTokens()

let rules = rulesParser(collectionAddress, core.namedCollections)
let assetsWithMetadata = await core.fetchCollectionAssetsRecursive(collectionAddress)
let assetsWithMetadataArray = []

let collectionAttributes = {}
let collectionAssetSize = 0

for (const [_, assetValues] of Object.entries(assetsWithMetadata)) {
  assetsWithMetadataArray.push(assetValues)

  for (const [attributeName, attributeValue] of Object.entries(assetValues.metadata)) {
    if (!rules.ignoreMetadataRarity.includes(attributeName)) {
      if(!collectionAttributes[attributeName]) {
        collectionAttributes[attributeName] = {}
      }

      if(!collectionAttributes[attributeName][attributeValue]) {
        collectionAttributes[attributeName][attributeValue] = 1
      } else {
        collectionAttributes[attributeName][attributeValue]++
      }
    }
  }

  collectionAssetSize+= assetValues.amount
}

for (const [_, assetValues] of Object.entries(assetsWithMetadata)) {
  assetValues.qualityScore = 0
 
  for (const [attributeName, attributeValue] of Object.entries(assetValues.metadata)) {
    if (!rules.ignoreMetadataRarity.includes(attributeName)) {
      let defaultItemScore = 1

      assetValues.qualityScore += (defaultItemScore / collectionAttributes[attributeName][attributeValue]) / collectionAssetSize
    }
  }
}

assetsWithMetadataArray.sort((c1, c2) => {
  return (c1.qualityScore < c2.qualityScore) ? 1 : -1
})

let rankCounter = 1

assetsWithMetadataArray.forEach((asset) => {
  asset.rank = rankCounter++
})

// filename, save on push, human readable db file, separator
let collectionAssetMetadata = new JsonDB(new Config(`./data/collectionAssetMetadata/${collectionAddress}`, true, false, "/"));

collectionAssetMetadata.push("/", assetsWithMetadata, true)

let collectionData = new JsonDB(new Config(`./data/collectionData/${collectionAddress}`, true, false, "/"));

collectionData.push("/", 
  {
    attributes: collectionAttributes,
    assetCount: collectionAssetSize
  }, true)