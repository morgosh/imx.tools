// more info on https://docs.google.com/document/d/1NvTaBfnpnuGrdC0rTVrxvO_X8mZIgVmslvKjIOTv37M

import fetch from "node-fetch";

let maxRPM = process.argv[2] ?? 2000
let minRPM = 60
let timeFrame = 600000
let cRPM
let url =  new URL("https://api.x.immutable.com/v1/orders")
let rlErr = "Unexpected end of JSON"

function getTimeDiffInMs(start) {
  const NS_PER_SEC = 1e9
  const NS_TO_MS = 1e6
  const diff = process.hrtime(start)

  return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS
}

async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// TBD: cancel rendundant requests once control.break is true
function recursiveRequests(delay, limited, start, maxDur, res, rej, control = {break: false}) {
  if (!control.break) {
    setTimeout(async () => {
      recursiveRequests(delay, limited, start, maxDur, res, rej, control)
      let wasRL = false;
      let wasErr = false;

      try {
        let response = await fetch(url)

        await response.json()
      } catch (e) {
        wasErr = true;

        if (e.toString().includes(rlErr)) wasRL = true;
      }

      if ((wasRL && !limited) || (!wasErr && limited)) {
        res()
        control.break = true
      }

      if (maxDur && getTimeDiffInMs(start) > maxDur) {
        rej("Timeframe exceeded")
        control.break = true
      }
    }, delay)
  }
}

async function testRL(rpm, limited, maxDur = null) {
  let delay = Math.ceil(60000/rpm)
  let start = process.hrtime()
  let rr = new Promise((res, rej) => {
    recursiveRequests(delay, limited, start, maxDur, res, rej)
  })
  let r = null

  await rr.catch((_) => {
    r = -1
  });
  r = r ? r : getTimeDiffInMs(start)

  if (limited) sleep(30000) // get out of limits

  return r
}

process.stdout.write("testing...")
let limitKickIn = await testRL(maxRPM * 10, false, timeFrame)

process.stdout.write(".")
let limitDisappeaIn = await testRL(12, true)

timeFrame = limitDisappeaIn + limitKickIn

console.log(`\nLimits kick in after ${Math.ceil(limitKickIn/1000)}s and are cleared after ${Math.ceil(timeFrame/1000)}s \n`)

while (maxRPM - minRPM > 50) {
  process.stdout.write(".")
  cRPM = Math.floor((maxRPM + minRPM)/2)
  limitKickIn = await testRL(cRPM, false, Math.ceil(timeFrame * 4/3))
  process.stdout.write(".")

  if (limitKickIn < 0) {
    minRPM = cRPM
  } else {
    maxRPM = cRPM
  }

  await testRL(12, true)
}

console.log(`\nYou can spam ${cRPM} requests per minute.`)