export default class ImxRules {
  static getAssetNameId(asset) {
    return asset.name
  }
  static getAssetMetadata(_) {
    return null
  }
  static getImageUrl(_) {
    return "undefined.jpg"
  }
  // some collections dont have any metadata filters enabled, we have to search by default search
  static getAssetSellTokenName(_) {
    return null
  }
  static parseQuality(_) {
    return "undefined"
  }

  static getLowestQualityName(asset) {
    return asset.nameId
  }

  //metadata ignores so we don't put in db
  static ignoreMetadata = [
    "name",
    "image_url",
    "image",
    "description"
  ]
  //metadata ignores so we don't use to calculate rarity // ignoreMetadata is usually handled before
  static ignoreMetadataRarity = [
  ]

  // we use additional in extended classes
  //metadata ignores so we don't put in db
  static additionalIgnoreMetadata = [
  ]
  //metadata ignores so we don't use to calculate rarity
  static additionalIgnoreMetadataRarity = [
  ]

  static getMetadataIgnores() {
    return [...this.ignoreMetadata, ...this.additionalIgnoreMetadata]
  }
  static getMetadataRarityIgnores() {
    return [...this.getMetadataIgnores(), ...this.ignoreMetadataRarity, ...this.additionalIgnoreMetadataRarity]
  }
}